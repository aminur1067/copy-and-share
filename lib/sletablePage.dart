import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class sletablePage extends StatefulWidget {
  const sletablePage({Key? key}) : super(key: key);

  @override
  State<sletablePage> createState() => _sletablePageState();
}

class _sletablePageState extends State<sletablePage> {

  TextEditingController TextControllar=TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(child: TextField(
              controller:TextControllar ,
              decoration: InputDecoration(
                hintText: 'your text'
              ),
            ),

            ),
            Expanded(
              flex: 3,
                child: SelectableText(
              'The word “Allah” predates Islam. Each Arab tribe had its own idol gods and goddesses, but a number of tribes also acknowledged the existence of an “unknown god” they called al-ilah, which literally means “the god.” He was considered to be the invisible, supreme deity; however, they did not have a unified concept of who he was. In due time, Allah became “…a universalization of the tribal god who was often referred to as al-ilah (the god). When the tribe encountered another tribe who had a god whom they also referred to as al-ilah, they both thought that they were referring to the same being, and so a universal idea of Allah grew among the Arabs.” (Nazir Ali, p. 26)',
              style: TextStyle(),
                  scrollPhysics:BouncingScrollPhysics(

                  ) ,
                  toolbarOptions: ToolbarOptions(
                    copy: true,
                    cut: true,
                    paste: true,
                    selectAll: true,
                  ),
                  showCursor:true ,

            ),)
          ],
        ),
      ),
    ));
  }
}
