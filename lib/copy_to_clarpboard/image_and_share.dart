import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:share_plus/share_plus.dart';

import 'imageShowcose.dart';

class copytoclipboard extends StatefulWidget {
  const copytoclipboard({Key? key}) : super(key: key);

  @override
  State<copytoclipboard> createState() => _copytoclipboardState();
}

class _copytoclipboardState extends State<copytoclipboard> {
  String text = "";
  String Subject = "";
  List<String> imagePath = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextFormField(
              decoration:
                  InputDecoration(labelText: 'Text', hintText: 'Your text'),
              onChanged: (value) {
                setState(() {
                  text = value;
                });
              },
            ),
            TextFormField(
              decoration: InputDecoration(
                  labelText: 'subject', hintText: 'Your subject'),
              onChanged: (value) {
                setState(() {
                  Subject = value;
                });
              },
            ),
            SizedBox(height: 10,),

            //imageShowcose
            ImageShowcase(
              onDelete: deleteImage,
              imagePath: imagePath,
            ),

            ListTile(

              title: Text('add image'),
              leading: Icon(Icons.add),
              onTap: () async {
                final imagePicker = ImagePicker();
                final pickedFile =
                    await imagePicker.pickImage(source: ImageSource.gallery);
                if (pickedFile != null) {
                  setState(() {
                    imagePath.add(pickedFile.path);
                    print(imagePath);
                  });
                }
              },
            ),
            SizedBox(height: 10,),
            ElevatedButton(onPressed: text.isEmpty && imagePath.isEmpty? null:(){share();}, child: Text('Share'))
          ],
        ),
      ),
    );
  }
  void share()async{
  if(imagePath.isEmpty){
    await Share.share(
      text,subject:Subject
    );
  }   else{
    await Share.shareFiles(imagePath,text: text,subject: Subject);
  }
  }
   void deleteImage (position){
    setState(() {
      imagePath.removeAt(position);
    });
   }
}
